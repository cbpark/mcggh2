/*
 *  Copyright (C) 2018 Chan Beom Park <cbpark@gmail.com>
 */

#include "utils.h"
#include <cstdlib>

namespace mcggh2 {
double getRandom() { return std::rand() / static_cast<double>(RAND_MAX); }
}  // namespace mcggh2
