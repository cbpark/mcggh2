/*
 *  Copyright (C) 2018 Chan Beom Park <cbpark@gmail.com>
 */

#ifndef MCGGH_SRC_UTILS_H_
#define MCGGH_SRC_UTILS_H_

namespace mcggh2 {
double getRandom();
}  // namespace mcggh2

#endif  // MCGGH_SRC_UTILS_H_
